﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    MonsterData data = null;
    
    MeshRenderer renderer;

    public float MaxHp { get; private set; }
    float currentHp;

    void Start()
    {
        MaxHp = data.hp;
        currentHp = MaxHp;

        renderer = gameObject.GetComponent<MeshRenderer>();
        renderer.material.color = data.color;
    }

    public void Damage(float damage)
    {
        currentHp -= damage;

        if (CheckDeathConditions())
            Destroy(gameObject);
    }

    private bool CheckDeathConditions()
    {
        if (currentHp <= 0)
        {
            Debug.Log("Unit is dead!", gameObject);
            return true;
        }

        return false;
    }
}
