﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToDamage : MonoBehaviour
{
    public float damage = 10;
    public void ApplyDamage()
    {
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            Debug.Log(hit.collider.tag);
            if (hit.collider.tag == "Damageable")
            {
                Debug.Log("hit!");
                hit.collider.gameObject.GetComponent<Enemy>().Damage(damage);
            }
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ApplyDamage();
        }
    }
}
