﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavAgentController : MonoBehaviour
{
    [SerializeField]
    Transform destination = null;

    [SerializeField]
    MonsterData data = null;

    NavMeshAgent agent;

    float hp;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(destination.position);
        agent.speed = data.speed;
    }

}
